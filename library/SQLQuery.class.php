<?php
 
class SQLQuery {
    protected $_dbHandle;
    protected $_result;
 
    /** Connects to database **/
 
    function connect($address, $account, $pwd, $name) {
        $this->_dbHandle = @mysql_connect($address, $account, $pwd);
        if ($this->_dbHandle != 0) {
            if (mysql_select_db($name, $this->_dbHandle)) {
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            return 0;
        }
    }
 
    /** Disconnects from database **/
 
    function disconnect() {
        if (@mysql_close($this->_dbHandle) != 0) {
            return 1;
        }  else {
            return 0;
        }
    }

    function delete($id = null) {
      if($id){
        $query = "DELETE FROM ".$this->_table." WHERE id =". $id; 
      } else { 
        $query = "DELETE FROM ".$this->_table;
      } 
        $result = mysql_query($query, $this->_dbHandle);
        if($result){
            return true;
        } else {
            return false;
        } 
    }
     
    function selectAll() {
        $query = 'select * from `'.$this->_table.'`';
        return $this->query($query);
    }
     
    function select($id){
        $query = 'select * from `'.$this->_table.'` where `id` = \''.mysql_real_escape_string($id).'\'';
        return $this->query($query, 1);    
    }
 
     
    /** Custom SQL Query **/
 
    function query($query, $singleResult = 0) {
 
        $this->_result = mysql_query($query, $this->_dbHandle);
        if($this -> _result){
            $result = array();
            while($row =mysql_fetch_array($this->_result, MYSQL_ASSOC))
            {
                $result[] = $row;
            }
            return $result; 
        }
    }
 
    /** Get number of rows **/
    function getNumRows() {
        return mysql_num_rows($this->_result);
    }
 
    /** Free resources allocated by a query **/
 
    function freeResult() {
        mysql_free_result($this->_result);
    }
 
    /** Get error string **/
 
    function getError() {
        return mysql_error($this->_dbHandle);
    }
}