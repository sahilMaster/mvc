<?php
class Controller {
     
    protected $_model;
    protected $_controller;
    protected $_action;
    protected $_template;
 
    function __construct($model, $controller, $action) {
         
        $this->_controller = $controller;
        $this->_action = $action;
        $this->_model = $model;
        $this->_template = new Template($controller,$action);
 
    }
 
    function set($name,$value) {
        $this->_template->set($name,$value);
    }
 
    function loadModel($modelName) {
        $this->model = new $modelName();
        return $this->model;
    }

    function loadTemplate($template){
        $this->_template->render();
        return $this->_template;
    } 
         
}